﻿namespace WindowsFormsApplication1
{
    partial class SalesContinue
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.productidTB = new System.Windows.Forms.TextBox();
            this.amountTB = new System.Windows.Forms.TextBox();
            this.nextBTN = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // productidTB
            // 
            this.productidTB.Location = new System.Drawing.Point(208, 35);
            this.productidTB.Name = "productidTB";
            this.productidTB.Size = new System.Drawing.Size(100, 20);
            this.productidTB.TabIndex = 0;
            // 
            // amountTB
            // 
            this.amountTB.Location = new System.Drawing.Point(208, 76);
            this.amountTB.Name = "amountTB";
            this.amountTB.Size = new System.Drawing.Size(100, 20);
            this.amountTB.TabIndex = 0;
            // 
            // nextBTN
            // 
            this.nextBTN.Location = new System.Drawing.Point(208, 131);
            this.nextBTN.Name = "nextBTN";
            this.nextBTN.Size = new System.Drawing.Size(75, 23);
            this.nextBTN.TabIndex = 1;
            this.nextBTN.Text = "NEXT";
            this.nextBTN.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(84, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Product ID";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(84, 76);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Amount";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.nextBTN);
            this.panel1.Controls.Add(this.amountTB);
            this.panel1.Controls.Add(this.productidTB);
            this.panel1.Location = new System.Drawing.Point(131, 49);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(356, 217);
            this.panel1.TabIndex = 1;
            // 
            // SalesManPage2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(619, 314);
            this.Controls.Add(this.panel1);
            this.Name = "SalesManPage2";
            this.Text = "SalesManPage2";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox productidTB;
        private System.Windows.Forms.TextBox amountTB;
        private System.Windows.Forms.Button nextBTN;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
    }
}