﻿namespace WindowsFormsApplication1
{
    partial class AddNewSalesman
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.nameTB = new System.Windows.Forms.TextBox();
            this.mobilenumberTB = new System.Windows.Forms.TextBox();
            this.salesusernameTB = new System.Windows.Forms.TextBox();
            this.salespasswordTB = new System.Windows.Forms.TextBox();
            this.addBTN = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.retypepasswordTB = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.retypepasswordTB);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.addBTN);
            this.panel1.Controls.Add(this.salespasswordTB);
            this.panel1.Controls.Add(this.salesusernameTB);
            this.panel1.Controls.Add(this.mobilenumberTB);
            this.panel1.Controls.Add(this.nameTB);
            this.panel1.Location = new System.Drawing.Point(70, 82);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(552, 282);
            this.panel1.TabIndex = 0;
            // 
            // nameTB
            // 
            this.nameTB.Location = new System.Drawing.Point(287, 33);
            this.nameTB.Name = "nameTB";
            this.nameTB.Size = new System.Drawing.Size(100, 20);
            this.nameTB.TabIndex = 0;
            // 
            // mobilenumberTB
            // 
            this.mobilenumberTB.Location = new System.Drawing.Point(287, 82);
            this.mobilenumberTB.Name = "mobilenumberTB";
            this.mobilenumberTB.Size = new System.Drawing.Size(100, 20);
            this.mobilenumberTB.TabIndex = 0;
            // 
            // salesusernameTB
            // 
            this.salesusernameTB.Location = new System.Drawing.Point(287, 128);
            this.salesusernameTB.Name = "salesusernameTB";
            this.salesusernameTB.Size = new System.Drawing.Size(100, 20);
            this.salesusernameTB.TabIndex = 0;
            // 
            // salespasswordTB
            // 
            this.salespasswordTB.Location = new System.Drawing.Point(287, 168);
            this.salespasswordTB.Name = "salespasswordTB";
            this.salespasswordTB.Size = new System.Drawing.Size(100, 20);
            this.salespasswordTB.TabIndex = 0;
            // 
            // addBTN
            // 
            this.addBTN.Location = new System.Drawing.Point(312, 241);
            this.addBTN.Name = "addBTN";
            this.addBTN.Size = new System.Drawing.Size(75, 23);
            this.addBTN.TabIndex = 1;
            this.addBTN.Text = "ADD";
            this.addBTN.UseVisualStyleBackColor = true;
            this.addBTN.Click += new System.EventHandler(this.addBTN_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(165, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(165, 82);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Mobile Number";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(165, 128);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Username";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(165, 168);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Password";
            // 
            // retypepasswordTB
            // 
            this.retypepasswordTB.Location = new System.Drawing.Point(287, 205);
            this.retypepasswordTB.Name = "retypepasswordTB";
            this.retypepasswordTB.Size = new System.Drawing.Size(100, 20);
            this.retypepasswordTB.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(167, 205);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(90, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Retype Password";
            // 
            // AddNewSalesman
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(665, 386);
            this.Controls.Add(this.panel1);
            this.Name = "AddNewSalesman";
            this.Text = "AddNewSalesman";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox retypepasswordTB;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button addBTN;
        private System.Windows.Forms.TextBox salespasswordTB;
        private System.Windows.Forms.TextBox salesusernameTB;
        private System.Windows.Forms.TextBox mobilenumberTB;
        private System.Windows.Forms.TextBox nameTB;
    }
}