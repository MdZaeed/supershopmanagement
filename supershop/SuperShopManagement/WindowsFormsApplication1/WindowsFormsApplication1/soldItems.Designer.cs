﻿namespace WindowsFormsApplication1
{
    partial class SoldItems
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.endDateDTP = new System.Windows.Forms.DateTimePicker();
            this.startDateDTP = new System.Windows.Forms.DateTimePicker();
            this.dailyBTN = new System.Windows.Forms.Button();
            this.checkBTN = new System.Windows.Forms.Button();
            this.backBTN = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.checkBTN);
            this.panel1.Controls.Add(this.dailyBTN);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.startDateDTP);
            this.panel1.Controls.Add(this.endDateDTP);
            this.panel1.Location = new System.Drawing.Point(77, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(777, 66);
            this.panel1.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(253, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(16, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "to";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(216, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Check Sell From";
            // 
            // endDateDTP
            // 
            this.endDateDTP.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.endDateDTP.Location = new System.Drawing.Point(287, 34);
            this.endDateDTP.Name = "endDateDTP";
            this.endDateDTP.Size = new System.Drawing.Size(183, 20);
            this.endDateDTP.TabIndex = 8;
            // 
            // startDateDTP
            // 
            this.startDateDTP.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.startDateDTP.Location = new System.Drawing.Point(28, 34);
            this.startDateDTP.Name = "startDateDTP";
            this.startDateDTP.Size = new System.Drawing.Size(183, 20);
            this.startDateDTP.TabIndex = 7;
            // 
            // dailyBTN
            // 
            this.dailyBTN.Location = new System.Drawing.Point(622, 21);
            this.dailyBTN.Name = "dailyBTN";
            this.dailyBTN.Size = new System.Drawing.Size(135, 32);
            this.dailyBTN.TabIndex = 11;
            this.dailyBTN.Text = "Check Today\'s Sell";
            this.dailyBTN.UseVisualStyleBackColor = true;
            this.dailyBTN.Click += new System.EventHandler(this.dailyBTN_Click);
            // 
            // checkBTN
            // 
            this.checkBTN.Location = new System.Drawing.Point(486, 29);
            this.checkBTN.Name = "checkBTN";
            this.checkBTN.Size = new System.Drawing.Size(75, 23);
            this.checkBTN.TabIndex = 12;
            this.checkBTN.Text = "Check";
            this.checkBTN.UseVisualStyleBackColor = true;
            this.checkBTN.Click += new System.EventHandler(this.checkBTN_Click);
            // 
            // backBTN
            // 
            this.backBTN.Location = new System.Drawing.Point(779, 329);
            this.backBTN.Name = "backBTN";
            this.backBTN.Size = new System.Drawing.Size(75, 23);
            this.backBTN.TabIndex = 7;
            this.backBTN.Text = "Go Back";
            this.backBTN.UseVisualStyleBackColor = true;
            this.backBTN.Click += new System.EventHandler(this.backBTN_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dataGridView1);
            this.panel2.Location = new System.Drawing.Point(77, 87);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(777, 222);
            this.panel2.TabIndex = 8;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(151, 17);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(474, 179);
            this.dataGridView1.TabIndex = 0;
            // 
            // SoldItems
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(916, 364);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.backBTN);
            this.Controls.Add(this.panel1);
            this.Name = "SoldItems";
            this.Text = "soldItems";
            this.Load += new System.EventHandler(this.SoldItems_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button dailyBTN;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker startDateDTP;
        private System.Windows.Forms.DateTimePicker endDateDTP;
        private System.Windows.Forms.Button checkBTN;
        private System.Windows.Forms.Button backBTN;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView dataGridView1;
    }
}