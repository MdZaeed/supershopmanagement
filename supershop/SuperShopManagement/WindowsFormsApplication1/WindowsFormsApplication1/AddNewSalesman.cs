﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace WindowsFormsApplication1
{
    public partial class AddNewSalesman : Form
    {
        public AddNewSalesman()
        {
            InitializeComponent();
        }
        public void ini()
        {
            
            nameTB.Text = "";
            mobilenumberTB.Text = "";
            salesusernameTB.Text = "";
            salespasswordTB.Text = "";
            retypepasswordTB.Text = "";
        }

        private void addBTN_Click(object sender, EventArgs e)
        {
            String name = nameTB.Text;
            String mobileNumber = mobilenumberTB.Text;
            String username = salesusernameTB.Text;
            String password = salespasswordTB.Text;
            String retypePassword = retypepasswordTB.Text;

            MySqlConnection con = DBConnect.Initialize();

            if (password == retypePassword)
            {
                if(name == "" || mobileNumber == "" || username == "" || password == "" || retypePassword == "")
                {
                    MessageBox.Show("Field cannot be empty!");
                }
                else
                {
                    con.Open();
                    String sql = "";
                    sql = "INSERT INTO `salesman_info` (`id`, `sales_username`, `sales_password`, `name`, `mobile_number`) VALUES (NULL, '"+username+ "', '"+password+ "', '" + name + "', '" + mobileNumber + "')";
                    MySqlCommand cmd = new MySqlCommand(sql, con);
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("New Salesman Information successfully inserted");
                    con.Close();
                    ini();
                }
            }
            else
            {
                MessageBox.Show("Passwords didn\'t match");
                return;
            }
                
        }
    }
}
