﻿namespace WindowsFormsApplication1
{
    partial class SalesStart
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.dateTB = new System.Windows.Forms.TextBox();
            this.customernumberTB = new System.Windows.Forms.TextBox();
            this.productnumberTB = new System.Windows.Forms.TextBox();
            this.logoutBTN = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.logoutBTN);
            this.panel1.Controls.Add(this.productnumberTB);
            this.panel1.Controls.Add(this.customernumberTB);
            this.panel1.Controls.Add(this.dateTB);
            this.panel1.Location = new System.Drawing.Point(127, 70);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(356, 217);
            this.panel1.TabIndex = 0;
            // 
            // dateTB
            // 
            this.dateTB.Location = new System.Drawing.Point(208, 35);
            this.dateTB.Name = "dateTB";
            this.dateTB.Size = new System.Drawing.Size(100, 20);
            this.dateTB.TabIndex = 0;
            // 
            // customernumberTB
            // 
            this.customernumberTB.Location = new System.Drawing.Point(208, 76);
            this.customernumberTB.Name = "customernumberTB";
            this.customernumberTB.Size = new System.Drawing.Size(100, 20);
            this.customernumberTB.TabIndex = 0;
            // 
            // productnumberTB
            // 
            this.productnumberTB.Location = new System.Drawing.Point(208, 127);
            this.productnumberTB.Name = "productnumberTB";
            this.productnumberTB.Size = new System.Drawing.Size(100, 20);
            this.productnumberTB.TabIndex = 0;
            // 
            // logoutBTN
            // 
            this.logoutBTN.Location = new System.Drawing.Point(235, 176);
            this.logoutBTN.Name = "logoutBTN";
            this.logoutBTN.Size = new System.Drawing.Size(75, 23);
            this.logoutBTN.TabIndex = 1;
            this.logoutBTN.Text = "LOGOUT";
            this.logoutBTN.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(84, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Date";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(84, 76);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Customer Number";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(84, 127);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(101, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Number of Products";
            // 
            // SalesManPage1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(622, 332);
            this.Controls.Add(this.panel1);
            this.Name = "SalesManPage1";
            this.Text = "SalesManPage1";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button logoutBTN;
        private System.Windows.Forms.TextBox productnumberTB;
        private System.Windows.Forms.TextBox customernumberTB;
        private System.Windows.Forms.TextBox dateTB;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}