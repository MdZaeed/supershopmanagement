﻿namespace WindowsFormsApplication1
{
    partial class Profit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.endDateDTP = new System.Windows.Forms.DateTimePicker();
            this.startDateDTP = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.checkBTN = new System.Windows.Forms.Button();
            this.dailyBTN = new System.Windows.Forms.Button();
            this.backBTN = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.endDateDTP);
            this.panel1.Controls.Add(this.startDateDTP);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.checkBTN);
            this.panel1.Controls.Add(this.dailyBTN);
            this.panel1.Location = new System.Drawing.Point(90, 53);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(604, 208);
            this.panel1.TabIndex = 0;
            // 
            // endDateDTP
            // 
            this.endDateDTP.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.endDateDTP.Location = new System.Drawing.Point(300, 114);
            this.endDateDTP.Name = "endDateDTP";
            this.endDateDTP.Size = new System.Drawing.Size(200, 20);
            this.endDateDTP.TabIndex = 4;
            // 
            // startDateDTP
            // 
            this.startDateDTP.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.startDateDTP.Location = new System.Drawing.Point(300, 49);
            this.startDateDTP.Name = "startDateDTP";
            this.startDateDTP.Size = new System.Drawing.Size(200, 20);
            this.startDateDTP.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(344, 85);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(20, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "To";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(310, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Check Profit From";
            // 
            // checkBTN
            // 
            this.checkBTN.Location = new System.Drawing.Point(313, 162);
            this.checkBTN.Name = "checkBTN";
            this.checkBTN.Size = new System.Drawing.Size(100, 23);
            this.checkBTN.TabIndex = 0;
            this.checkBTN.Text = "Check Profit";
            this.checkBTN.UseVisualStyleBackColor = true;
            this.checkBTN.Click += new System.EventHandler(this.checkBTN_Click);
            // 
            // dailyBTN
            // 
            this.dailyBTN.Location = new System.Drawing.Point(34, 49);
            this.dailyBTN.Name = "dailyBTN";
            this.dailyBTN.Size = new System.Drawing.Size(149, 49);
            this.dailyBTN.TabIndex = 0;
            this.dailyBTN.Text = "Check Daily Profit";
            this.dailyBTN.UseVisualStyleBackColor = true;
            this.dailyBTN.Click += new System.EventHandler(this.dailyBTN_Click);
            // 
            // backBTN
            // 
            this.backBTN.Location = new System.Drawing.Point(317, 267);
            this.backBTN.Name = "backBTN";
            this.backBTN.Size = new System.Drawing.Size(75, 23);
            this.backBTN.TabIndex = 1;
            this.backBTN.Text = "Go Back";
            this.backBTN.UseVisualStyleBackColor = true;
            this.backBTN.Click += new System.EventHandler(this.backBTN_Click);
            // 
            // Profit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(792, 316);
            this.Controls.Add(this.backBTN);
            this.Controls.Add(this.panel1);
            this.Name = "Profit";
            this.Text = "Profit";
            this.Load += new System.EventHandler(this.Profit_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DateTimePicker endDateDTP;
        private System.Windows.Forms.DateTimePicker startDateDTP;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button checkBTN;
        private System.Windows.Forms.Button dailyBTN;
        private System.Windows.Forms.Button backBTN;
    }
}